[console_scripts]
runjsonrpc2 = jsonrpc2.cmd:main

[paste.app_factory]
main = jsonrpc2.paste:make_app

[paste.paster_create_template]
paster_jsonrpc2 = jsonrpc2.paste.templates:JsonRpcTemplate

